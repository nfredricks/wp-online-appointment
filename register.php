<?php
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/online-appointment/email.php');


if(isset($_POST['owners'])){
    register();
}
else{
    wp_redirect( home_url()."/sessions" ); exit;
}


function register(){
    global $wpdb;
    $table_name = $wpdb->prefix . "online_appt";
    $current_user = wp_get_current_user();
    $id = $current_user->user_login;
    $school = get_user_meta($current_user->ID, 'school', true);

    $appt_time = strip_tags($_POST['appt_time']);
    $appt_date = strip_tags($_POST['appt_date']);
    $subject = str_replace(',', '', strip_tags($_POST['subject']));
    $comments = str_replace(',', '',strip_tags($_POST['comment']));
    $tutor = str_replace(',', '',strip_tags($_POST['owners']));
    //grab picked owners string


    //FIXME check to see if $tutor is in the owners field and also that it has not already been added to the picked owners field
    //also check to see if $id has already applied for this date/time
    //and check to see if the $id has already signed up for one that day
    //stuff
    //stuff
    //stuff
    
    $registered_school = true;


    $result = get_option('widget_phs_widget');
    $result = array_values($result);
    $schools = preg_split('/\r\n|\r|\n/', $result[0]['link_titles']);
    $num_schools = count($schools);

    $sanitized_schools = array();
    for($i=0; $i<$num_schools; $i++){

        $sanitized_schools[] = strtolower(str_replace(' ','_',strip_tags($schools[$i])));
    }

    for($i=0; $i<$num_schools; $i++){
        if($school == $schools[$i]){
            $user_opt = $sanitized_schools[$i]."_users";
            $pass_opt = $sanitized_schools[$i]."_pass";
            $registered_school = true;
            break;
        }
        else{
            $registered_school = false;
        }
    }


    $days = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE appt_date = %s AND attendee != %s ", $appt_date, $nothing));

    $already_registered = false;
    for($i = 0; $i<count($days); $i++){
        $attend_array = explode(',', $days[$i]->attendee);
        for($j = 0; $j<count($attend_array); $j++){
            if($attend_array[$j] == $id){
                $already_registered = true;
            }
        }
    }
    if(!$already_registered){
        if($registered_school){

            $user_array = preg_split ('/\r\n|\r|\n/', get_option($user_opt));
            $pass_array = preg_split ('/\r\n|\r|\n/', get_option($pass_opt));

            testing($user_array);
            testing($pass_array);



            //we must determine what order number this person is for this day
            $nothing = '';
            for($i = 0; $i<count($days); $i++){ //loop over all of the rows of registered students
                $username_array = explode(',', $days[$i]->username);

                for($j =0; $j<count($username_array); $j++){ //loop over the usernames that are already used
                    $original_num = count($user_array);
                    for($k = 0; $k<$original_num; $k++){ //loop over all available usernames
                        if(trim($user_array[$k]) == trim($username_array[$j])){
                            unset($user_array[$k]); //if it is already used then drop it from the available usernames
                            unset($pass_array[$k]);
                        }
                    }
                    $user_array = array_merge($user_array); //re number array for next iteration
                    $pass_array = array_merge($pass_array); 
                }
            }

            if(count($user_array) > 0){ //if we still have available usernames...
                $username = $user_array[0];
                $password = $pass_array[0];
        

                $this_row = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE appt_time = %s AND appt_date = %s", $appt_time, $appt_date));
                $picked_owners = $this_row[0]->owner_picked;
                $cur_passwords = $this_row[0]->password;
                $cur_usernames = $this_row[0]->username;

                //append this tutor to string
                if($picked_owners != NULL){
                    $attendees = $this_row[0]->attendee;
                    $update = array('owner_picked' => $picked_owners.",".$tutor, 'attendee' => $attendees.",".$id, 'username' => $cur_usernames.",".$username, 'password' => $cur_passwords.",".$password );
                }
                else{
                    $update = array('owner_picked' => $tutor, 'attendee' => $id, 'username' => $username, 'password' => $password);
                }
                $where = array('appt_time' => $appt_time ,'appt_date' => $appt_date );
                //update picked owners
                $wpdb->update($table_name, $update, $where);


                registered_attendee($tutor, $id, $appt_time, $appt_date, $username, $password);

                if(registered_tutor($tutor, $id, $appt_time, $appt_date, $comments, $subject, $username)){
                    wp_redirect( home_url()."/sessions" ); exit;
                }
                
            }
            else{ //no more logins available
                wp_redirect(home_url()."/individual-sessions?appt_error=logins");
            }
        }
        else{//bad school


            wp_redirect(home_url()."/individual-sessions?appt_error=school");
        }

        //redirect to registered appointments
    }
    else{ //user has already registered for an appt today
        wp_redirect(home_url()."/individual-sessions?appt_error=duplicate");
    }

}

function testing(&$arr){
    $starting_len = count($arr);
    for($i=0; $i<$starting_len; $i++){
        if(trim($arr[$i]) == ''){
            unset($arr[$i]);
        }
    }
    $arr = array_merge($arr);
}


?>