$ = jQuery;
$(document).ready(function(){
	//find appt tables
	$('.appt-table').each(function(){
	//detect clicks and add class appt-registered to anchor tags (blue background)
		$(this).on('click',function(e){
			e.preventDefault();
			$(e.target).toggleClass('appt-registered-new').removeClass('not-avail');
		});
	});

	$('form#appt-admin').on('submit',function(){
		//loop through appt-registered anchor tags and build array of date and time
			//back up to parent tr, grab value of first child of parent tr (time), get child number of anchor, grab said child number of first tr (date)
		var opts = {};
		var table_ids = [];
		$("a.appt-registered-new").each(function(){
			$this = $(this);
			var table = $this.parents('table');
			table.toggleClass('appt-table');
			var table_id = table.attr('class');
			table_ids.push(table_id);
			table.toggleClass('appt-table');

			var time = $this.attr('data-time');
			var date = $this.attr('data-date');

			if (opts[table_id] == undefined) opts[table_id] = [];
			opts[table_id].push(date+","+time);

		});
		console.log(opts);

		//put array into input field
		j = 0;
		for(var opt in opts){
			var string_opts = opts[opt].join(" next ");
			var dom = "<input type='hidden' id='hidden"+j+"' name='hidden"+j+"' value='"+string_opts+"'>";
			$("input#appt_submit_hidden").after(dom);
			j++;
		}

		
		//submit
		return true;
	});
});
