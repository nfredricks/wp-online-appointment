<?php
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/online-appointment/email.php');
global $wpdb;
global $current_user;


$table_name = $wpdb->prefix . "online_appt";
$result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE appt_time=%s AND appt_date=%s",$_POST['time'], $_POST['date']));

$len = count($result);

$owners = explode(',', $result[0]->owner_picked);
$attendees = explode(',', $result[0]->attendee);
$usernames = explode(',', $result[0]->username);
$passwords = explode(',', $result[0]->password);
$picked_cron_list = explode(',', $result[0]->owner_cron); 
$attend_cron_list = explode(',', $result[0]->attendee_cron);

$at_len = count($attendees);
$del_owner = "";
$del_attend = "";
for($j=0; $j<$at_len; $j++){
    if($owners[$j] == $_POST['tutor']){
        //should make sure attendee == current user
        $picked_cron_index = array_search($owners[$j], $picked_cron_list);
        $attend_cron_index = array_search($attendees[$j], $attend_cron_list);

        if($picked_cron_index !== FALSE){
            unset($picked_cron_list[$picked_cron_index]);
        }
        if($attend_cron_index !== FALSE){
            unset($attend_cron_list[$attend_cron_index]);
        }
        $del_owner = $owners[$j];
        $del_attend = $attendees[$j];
        unset($attendees[$j]);
        unset($owners[$j]);
        unset($usernames[$j]);
        unset($passwords[$j]);

        break;
    }
}

$owners = implode(',', $owners);
$attendees = implode(',', $attendees);
$usernames = implode(',', $usernames);
$passwords = implode(',', $passwords);
$new_picked_cron_list = implode(',', $picked_cron_list);
$new_attend_cron_list = implode(',', $attend_cron_list);

$update = array('owner_picked' => $owners, 'attendee' => $attendees, 'username' => $usernames, 'password' => $passwords, 'owner_cron' => $new_picked_cron_list, 'attendee_cron' => $new_attend_cron_list);

$where = array('appt_time' => $_POST['time'] ,'appt_date' => $_POST['date'] );
//update picked owners and attendees
$wpdb->update($table_name, $update, $where);

de_registered_tutor($del_owner, $del_attend, $_POST['time'], $_POST['date']);




wp_redirect( home_url()."/sessions" ); exit;


?>