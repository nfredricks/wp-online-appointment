jQuery(document).ready(function($){
	//hide registration box

	//find appt tables
	appt_tables = $('.appt-table');

	appt_tables.each(function(){
	//detect clicks and add class appt-registered to anchor tags (blue background)
		$(this).on('click',function(e){
			e.preventDefault();
			var a = $(e.target);
			if(!a.hasClass('not-avail')){
				a.toggleClass('appt-registered-new');
				//if any anchor has class appt-registered-new then do nothing
				var as = appt_tables.children().find('.appt-registered-new');
				var select = $('select#owners');
				var input_date = $('input#appt_date');
				var input_time = $('input#appt_time');
				var owners,end, displays;
				if(as[1] == null){
					if(as[0] == null){
						//we just deselected one, need to delete out options, hide the registration and remove hidden field values
						select[0].innerHTML = "";
						input_date.attr('value', '');
						input_time.attr('value', '');
						$('.registration').slideUp(300, 'linear');
					}

					else{
						$('.registration').slideDown(300, 'linear');
							//remove week navigation
							//grab owners and put them in select box in register form
							owners = a.attr('data-owners');
                            displays = a.attr('data-displays');
							owners = owners.split(',');
                            displays = displays.match(/[^,]*\([^\(]*\)[^,]*|[^,]+/g);
							end = owners.length;
                
							for(i=0; i<end; i++){
								select.append('<option value="'+owners[i]+'">'+displays[i]+'</option>');
							}
							
							//put time and date info into hidden fields
							input_date.attr('value', a.attr('data-date'));
							input_time.attr('value', a.attr('data-time'));
					}
					
				}
				else{
					as.each(function(){
						$(this).removeClass('appt-registered-new');
					});
					a.toggleClass('appt-registered-new'); //make sure the new one is still selected
					owners = a.attr('data-owners');
					displays = a.attr('data-displays');

					owners = owners.split(',');
                    displays = displays.match(/[^,]*\([^\(]*\)[^,]*|[^,]+/g);

					select.html("");
					end = owners.length;
					for(i=0; i<end; i++){
						select.append('<option value='+owners[i]+'>'+displays[i]+'</option>');
					}
					//put time and date info into hidden fields
					input_date.attr('value', a.attr('data-date'));
					input_time.attr('value', a.attr('data-time'));

					//bring up message that says only one signup at a time
				}
			}
		});
	});
	$('#cal_shift').on('click', function(e){
		e.preventDefault();
		$this = $(this);
		words = $this.attr('data-words');
		$this.attr('data-words', $this.html());
		$this.html(words);
		table = $('table.appt-cal');
		height = table.css('height');
		$('#cal_container').css('height', height);
		console.log(words);
		if(words == "Last Week"){
			$('table.appt-cal-next').css({'position':'relative'});
			table.animate({'margin-top': '-'+height}, 500);
		}
		else{
			table.animate({'margin-top': 0}, 500);
		}
	});
    
    $("#registration-form").on('submit', function(){
        var checkThis = true;
        var $this = $(this);
        $this.find("input").each(function(){
            if(inputCheck($(this)) == false){
                checkThis = false;
            }
        });
        $this.find("textarea").each(function(){
            if(inputCheck($(this)) == false){
                checkThis = false;
            }
        });
        if(checkThis){
			var subButton = $this.find('input[type=submit]');
			subButton.attr('value', 'Please wait while we register you.');
			subButton.prop('disabled', true);

        }

        return checkThis;
    });
	
});

function inputCheck($this){
    if(jQuery.trim($this.attr('value')) == ""){
        $this.addClass('error');
        $this.siblings('label[for='+$this.attr('id')+']').find('span.required').addClass('error');
        return false;
    }
    else{
        $this.removeClass('error');
        $this.siblings('label[for='+$this.attr('id')+']').find('span.required').removeClass('error');
        return true;
    }
}
