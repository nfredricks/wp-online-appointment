<?php

function registered_tutor($tutor, $attend, $time, $date, $comment, $session_subject, $username){
	global $wpdb;
    //send email to tutor notifing them of attendee registration
	$tutor = get_user_by('login',$tutor);
	$attend = get_user_by('login', $attend);

   	$headers = 'From: Wolverine Tutors <registration@wolverinetutors.org>' . "\r\n";
   	$subject = "[Wolverine Tutors] Session Registration";
   	$email = $tutor->user_email;
   	$message = "<h2>Hello ".$tutor->display_name."</h2>";
   	$message .="<p>".$attend->display_name." (<a href='mailto:".$attend->user_email."'>".$attend->user_email."</a>) has registered for your tutoring session on ".$date." at ".$time."</p>";
    $message .= "<p>The student will have the username: ".$username."</p>";
   	$message .= "<h3>Session Subject</h3><p>".$session_subject."</p>";
   	$message .= "<h3>Session Comments</h3><p>".$comment."</p>";
   	add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
   	return wp_mail( $email, $subject, $message, $headers);
    
}
function registered_attendee($tutor, $attend, $time, $date, $username, $password){
  global $wpdb;
    //send email to tutor notifing them of attendee registration
  $tutor = get_user_by('login',$tutor);
  $attend = get_user_by('login', $attend);

  $subject = "[Wolverine Tutors] Session Registration";
  $email = $attend->user_email;
  $message = "<h2>Hello ".$attend->display_name."</h2>";
  $message .="<p>You have registered with ".$tutor->display_name." (<a href='mailto:".$tutor->user_email."'>".$tutor->user_email."</a>) for a tutoring session on ".$date." at ".$time."</p>";
  $message .='<p>Your login information is username: '.$username.' and password: '.$password.'</p>';
  $message .= nl2br(get_option('student_email_append'));


  add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
  return wp_mail( $email, $subject, $message);
}
function de_registered_attendee($tutor, $attend, $time, $date){
    //send email to attendee in case of tutor deregister


    return true;
}
function de_registered_tutor($tutor, $attend, $time, $date){
    //send email to tutor in case of attendee deregister
      global $wpdb;
    //send email to tutor notifing them of attendee registration
  $tutor = get_user_by('login',$tutor);
  $attend = get_user_by('login', $attend);

  $subject = "[Wolverine Tutors] Session Deregistration";
  $email = $tutor->user_email;
  $message = "<h2>Hello ".$tutor->display_name."</h2>";
  $message .="<p>".$attend->display_name." (<a href='mailto:".$attend->user_email."'>".$attend->user_email."</a>) has deregistered for your tutoring session on ".$date." at ".$time."</p>";


  add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
  return wp_mail( $email, $subject, $message);
}


?>