<?php
/*
Plugin Name: Online Appointment Manager
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: Allows users to schedule appointments for online interviews, tutoring sessions and or any other online event
Version: 1.1
Author: Nicholas Fredricks
Author URI: http://logikgate.com
License: GPL2

Copyright 2012  Nicholas Fredricks  (email : nfredricks@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/





add_filter('the_posts', 'conditionally_add_scripts_and_styles'); 
function conditionally_add_scripts_and_styles($posts){
    if (empty($posts)){ return $posts;}

    $shortcode_found = false; 
    foreach ($posts as $post) {
        if (stripos($post->post_content, '[appointments]') !== false || stripos($post->post_content, '[registered-appointments]') !== false ) {
            $shortcode_found = true; 
            break;
        }
    }
    if ($shortcode_found) {
        wp_enqueue_style('appt-cal-style', plugins_url('style.css', __FILE__));
        wp_enqueue_script('appt-cal-js', plugins_url('script.js', __FILE__), array('jquery'));
    }

 
    return $posts;
    

}




add_action( 'admin_menu', 'appt_manager' );
add_action('online_appt_reminders', 'online_appt_cron_emails');

function appt_manager() {
	$page = add_menu_page('My Plugin Options', 'Appt. Manager', 'manage_options', 'online-appointment-manager', 'appt_settings');
    add_action('admin_print_styles-' . $page, 'appt_manager_admin_styles');
}

function appt_manager_admin_styles() {
    wp_enqueue_style('appt-cal-style', plugins_url('style.css', __FILE__));
    wp_enqueue_script('appt-cal-admin-js', plugins_url('admin/script.js', __FILE__), 'jquery');
}


function appt_settings() {
    
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
 // variables for the field and option names 
    global $wpdb;
    global $current_user;
    $day_name = 'days_to_show';
    $st_time = 'appt_start_time';
    $fn_time = 'appt_finish_time';

    $result = get_option('widget_phs_widget');
    $result = array_values($result);
    $schools = preg_split('/\r\n|\r|\n/', $result[0]['link_titles']);
    $num_schools = count($schools);

    $sanitized_schools = array();
    $replaces = array(' ','.');
    $puts = array('_','');
    for($i=0; $i<$num_schools; $i++){

        $sanitized_schools[] = strtolower(str_replace($replaces,$puts,strip_tags($schools[$i])));
    }



    $email_append = 'student_email_append';
    $reminder_subject = 'online_appt_cron_email_subject';
    $reminder_body = 'online_appt_cron_email_body';
    $student_reminder_subject = 'online_appt_cron_email_subject_student';
    $student_reminder_body = 'online_appt_cron_email_body_student';
    $appt_loc = 'reg_appts_loc';
    $week_one = 'hidden0';
    $week_two = 'hidden1';
    $table_name = $wpdb->prefix . "online_appt";
    $hidden_field_name = 'appt_submit_hidden';

    // Read in existing option value from database
    $day_val = get_option( $day_name );
    $st_time_val = get_option( $st_time );
    $fn_time_val = get_option( $fn_time );

    $schools_user_val = array();
    $schools_pass_val = array();
    for($i=0; $i<$num_schools; $i++){
        $schools_user_val[] = get_option($sanitized_schools[$i]."_users");
        $schools_pass_val[] = get_option($sanitized_schools[$i]."_pass");
    }



    $email_append_val = get_option($email_append);
    $reminder_subject_val = get_option($reminder_subject);
    $reminder_body_val = get_option($reminder_body);
    $student_reminder_subject_val = get_option($student_reminder_subject);
    $student_reminder_body_val = get_option($student_reminder_body);

    $checker = false; //determine if the user is an admin
    $roles = $current_user->roles;
    $type = 'administrator';
    foreach($roles as $role){
      if($role == strtolower(trim($type))){
         $checker = true;
        }
    }

    // See if the user has posted us some information
    // If they did, this hidden field will be set to 'Y'
    
    //echo "Hours until today's emails ".((wp_next_scheduled( 'online_appt_reminders', array(true))-time())/60/60)."<br>";
    //echo "Hours until tomorrow's emails ".((wp_next_scheduled('online_appt_reminders', array(false))-time())/60/60);
    if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
        if($checker){

            $gofs = get_option( 'gmt_offset' ); // get WordPress offset in hours
            $hour_to_send = array(5,11,17);
            $args = array(array(true), array(true), array(false));

            for($i=0; $i<count($args); $i++){
                $timestamp = wp_next_scheduled( 'online_appt_reminders', $args[$i]);
                wp_unschedule_event($timestamp, 'online_appt_reminders', $args[$i]);
            }
            
            for($i=0; $i<count($hour_to_send); $i++){
                $hour = $hour_to_send[$i]-$gofs;
                $unix_time = mktime($hour,55,0);
                if($unix_time <= (time()+$gofs*60*60)){
                    $unix_time = $unix_time+(24*60*60);
                }


                wp_schedule_event($unix_time, 'daily', 'online_appt_reminders', $args[$i]); 
            }


             //online_appt_cron_emails();
        }
        // echo online_appt_cron_email();
        // Read their posted value
        $day_val = $_POST[ $day_name ];
        $st_time_val = $_POST[ $st_time ];
        $fn_time_val = $_POST[ $fn_time ];


        for($i=0; $i<$num_schools; $i++){
            $schools_pass_val[$i] = $_POST[$sanitized_schools[$i]."_pass"];
            $schools_user_val[$i] = $_POST[$sanitized_schools[$i]."_users"];
        }


        $email_append_val = $_POST[$email_append];
        $reminder_body_val = $_POST[$reminder_body];
        $reminder_subject_val = $_POST[$reminder_subject];
        $student_reminder_body_val = $_POST[$student_reminder_body];
        $student_reminder_subject_val = $_POST[$student_reminder_subject];


        $week_data = array();
        if(isset($_POST[ $week_one ])) array_push($week_data, $_POST[ $week_one ]);
        if(isset($_POST[ $week_two ])) array_push($week_data, $_POST[ $week_two ]);

        $user_name = $current_user->user_login;
        $display_name = $current_user->display_name;

        foreach($week_data as $data){
            $data = explode(' next ', $data);
            foreach($data as $datetime){

                $datetime = explode(",", $datetime);
                $appt_date = $datetime[0];
                $appt_time = $datetime[1];

                //check to see if appointment time and date already exist in database
                $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE appt_time = %s AND appt_date = %s", $appt_time, $appt_date));
                //if not then make a brand new entry with empty attendee inputs
                if($result == NULL){
                    $rows_affected = $wpdb->insert( $table_name, array( 'owner' => $user_name, 'time_stamp' => current_time('mysql'), 'appt_length' => '01:00:00', 'appt_time' => $appt_time, 'appt_date' => $appt_date, 'attendee' => '', 'subject' =>'', 'comment' => '') );  
                }

                //if so then just append $user_name to owner separated by commas
                else{
                    //check $result to see if user has already registered for this session, if so remove user
                    $owner_list = explode(',',$result[0]->owner);
                    $removed = false;
                    foreach($owner_list as $key => $reg_user){
                        if($reg_user == $user_name){ 
                            unset($owner_list[$key]); $removed = true;
                        }
                    }
                    $new_user_list = implode(',', $owner_list);

                    $picked_list = explode(',', $result[0]->owner_picked);
                    $attend_list = explode(',', $result[0]->attendee);
                    $username_list = explode(',',$result[0]->username);
                    $password_list = explode(',',$result[0]->password);
                    $picked_cron_list = explode(',', $result[0]->owner_cron); 
                    $attend_cron_list = explode(',', $result[0]->attendee_cron);


                    //if appointment has already been registered for we delete tutor out of picked_owners and remove attendee. We then email the attendee a notification of the deletion.
                    foreach($picked_list as $key => $picked_user){
                        if($picked_user == $user_name){ 
                            require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/online-appointment/email.php');
                            de_registered_attendee($picked_user, $attend_list[$key], $appt_time, $appt_date);

                            $picked_cron_index = array_search($picked_list[$key], $picked_cron_list);
                            $attend_cron_index = array_search($attend_list[$key], $attend_cron_list);

                            if($picked_cron_index !== FALSE){
                                unset($picked_cron_list[$picked_cron_index]);
                            }
                            if($attend_cron_index !== FALSE){
                                unset($attend_cron_list[$attend_cron_index]);
                            }

                            unset($picked_list[$key]); 
                            unset($attend_list[$key]);
                            unset($username_list[$key]);
                            unset($password_list[$key]);
                        }
                    }

                    $new_picked_list = implode(',', $picked_list);
                    $new_attend_list = implode(',', $attend_list);
                    $new_username_list = implode(',', $username_list);
                    $new_password_list = implode(',', $password_list);
                    $new_picked_cron_list = implode(',', $picked_cron_list);
                    $new_attend_cron_list = implode(',', $attend_cron_list);


                    //if no more owners are available for this timeslot we delete the entry
                    if($new_user_list == ""){
                        $wpdb->query( $wpdb->prepare("DELETE FROM $table_name WHERE appt_time = %s AND appt_date = %s", $appt_time, $appt_date));
                    }
                    else{
                        //if we didnt remove the user then add them to the owner list
                        if($removed == false) $update = array('owner' => $new_user_list.",".$user_name);
                        else{ 
                            $update = array('owner' => $new_user_list, 'owner_picked' => $new_picked_list, 'attendee' => $new_attend_list, 'username' => $new_username_list, 'password' => $new_password_list, 'owner_cron' => $new_picked_cron_list, 'attendee_cron' => $new_attend_cron_list);
                        }
                        $where = array('appt_time' => $appt_time ,'appt_date' => $appt_date );
                        $wpdb->update($table_name, $update, $where);
                    }
                }   

 
            }
        }
      

        // Save the posted value in the database
        if($st_time_val < $fn_time_val){
            update_option( $day_name, $day_val );
            update_option( $st_time, $st_time_val );
            update_option( $fn_time, $fn_time_val );


       

            for($i=0; $i<$num_schools; $i++){
                update_option( $sanitized_schools[$i]."_users", $schools_user_val[$i]);
                update_option( $sanitized_schools[$i]."_pass", $schools_pass_val[$i]);

            }


            update_option( $email_append, $email_append_val);
            update_option($reminder_body, $reminder_body_val);
            update_option($reminder_subject, $reminder_subject_val);
            update_option($student_reminder_body, $student_reminder_body_val);
            update_option($student_reminder_subject, $student_reminder_subject_val);

        // Put an settings updated message on the screen

?>
<div class="updated"><p><strong><?php _e('settings saved.'); ?></strong></p></div>
<?php
        }
        else{
?>
<div class="updated"><p><strong>Your start time must be before your finish time :[</strong></p></div>
<?php
        }

    }

    // Now display the settings editing screen

    echo '<div class="wrap">';

    // header

    echo "<h2>" . __( 'Online Appointment Manager', 'online_appt' ) . "</h2>";
    // settings form

    
?>
<form name="appt-admin" id="appt-admin" method="post" action="">
<input type="hidden" name="<?php echo $hidden_field_name; ?>" id="<?php echo $hidden_field_name; ?>" value="Y">
<div <?php if(!$checker){ echo "style='display:none;'";} ?> >
<p><?php _e("Show Weekends?", 'online_appt' ); ?> 
<select name="<?php echo $day_name; ?>">
    <option value="<?php echo $day_val; ?>"><?php echo $day_val; ?></option>
    <option value="<?php if($day_val == "yes"){echo "no";} else{ echo "yes";} ?>"><?php if($day_val == "yes"){echo "no";} else{ echo "yes";} ?></option>
</select>
</p>
<p><?php _e("At what time will you start taking appointments?", 'online_appt'); ?>
    <select name="<?php echo $st_time; ?>">
        <option value="0" <?php if($st_time_val == "0") echo "selected"; ?> >12 am</option>
        <option value="1" <?php if($st_time_val == "1") echo "selected"; ?> >1 am</option>
        <option value="2" <?php if($st_time_val == "2") echo "selected"; ?> >2 am</option>
        <option value="3" <?php if($st_time_val == "3") echo "selected"; ?> >3 am</option>
        <option value="4" <?php if($st_time_val == "4") echo "selected"; ?> >4 am</option>
        <option value="5" <?php if($st_time_val == "5") echo "selected"; ?> >5 am</option>
        <option value="6" <?php if($st_time_val == "6") echo "selected"; ?> >6 am</option>
        <option value="7" <?php if($st_time_val == "7") echo "selected"; ?> >7 am</option>
        <option value="8" <?php if($st_time_val == "8") echo "selected"; ?> >8 am</option>
        <option value="9" <?php if($st_time_val == "9") echo "selected"; ?> >9 am</option>
        <option value="10" <?php if($st_time_val == "10") echo "selected"; ?> >10 am</option>
        <option value="11" <?php if($st_time_val == "11") echo "selected"; ?> >11 am</option>
        <option value="12" <?php if($st_time_val == "12") echo "selected"; ?> >12 am</option>
        <option value="13" <?php if($st_time_val == "13") echo "selected"; ?> >1 pm</option>
        <option value="14" <?php if($st_time_val == "14") echo "selected"; ?> >2 pm</option>
        <option value="15" <?php if($st_time_val == "15") echo "selected"; ?> >3 pm</option>
        <option value="16" <?php if($st_time_val == "16") echo "selected"; ?> >4 pm</option>
        <option value="17" <?php if($st_time_val == "17") echo "selected"; ?> >5 pm</option>
        <option value="18" <?php if($st_time_val == "18") echo "selected"; ?> >6 pm</option>
        <option value="19" <?php if($st_time_val == "19") echo "selected"; ?> >7 pm</option>
        <option value="20" <?php if($st_time_val == "20") echo "selected"; ?> >8 pm</option>
        <option value="21" <?php if($st_time_val == "21") echo "selected"; ?> >9 pm</option>
        <option value="22" <?php if($st_time_val == "22") echo "selected"; ?> >10 pm</option>
        <option value="23" <?php if($st_time_val == "23") echo "selected"; ?> >11 pm</option>
    </select>
</p>
<p><?php _e("At what time will your last appointment of the day occur?", 'online_appt'); ?>
    <select name="<?php echo $fn_time; ?>">
        <option value="0" <?php if($fn_time_val == "0") echo "selected"; ?> >12 am</option>
        <option value="1" <?php if($fn_time_val == "1") echo "selected"; ?> >1 am</option>
        <option value="2" <?php if($fn_time_val == "2") echo "selected"; ?> >2 am</option>
        <option value="3" <?php if($fn_time_val == "3") echo "selected"; ?> >3 am</option>
        <option value="4" <?php if($fn_time_val == "4") echo "selected"; ?> >4 am</option>
        <option value="5" <?php if($fn_time_val == "5") echo "selected"; ?> >5 am</option>
        <option value="6" <?php if($fn_time_val == "6") echo "selected"; ?> >6 am</option>
        <option value="7" <?php if($fn_time_val == "7") echo "selected"; ?> >7 am</option>
        <option value="8" <?php if($fn_time_val == "8") echo "selected"; ?> >8 am</option>
        <option value="9" <?php if($fn_time_val == "9") echo "selected"; ?> >9 am</option>
        <option value="10" <?php if($fn_time_val == "10") echo "selected"; ?> >10 am</option>
        <option value="11" <?php if($fn_time_val == "11") echo "selected"; ?> >11 am</option>
        <option value="12" <?php if($fn_time_val == "12") echo "selected"; ?> >12 am</option>
        <option value="13" <?php if($fn_time_val == "13") echo "selected"; ?> >1 pm</option>
        <option value="14" <?php if($fn_time_val == "14") echo "selected"; ?> >2 pm</option>
        <option value="15" <?php if($fn_time_val == "15") echo "selected"; ?> >3 pm</option>
        <option value="16" <?php if($fn_time_val == "16") echo "selected"; ?> >4 pm</option>
        <option value="17" <?php if($fn_time_val == "17") echo "selected"; ?> >5 pm</option>
        <option value="18" <?php if($fn_time_val == "18") echo "selected"; ?> >6 pm</option>
        <option value="19" <?php if($fn_time_val == "19") echo "selected"; ?> >7 pm</option>
        <option value="20" <?php if($fn_time_val == "20") echo "selected"; ?> >8 pm</option>
        <option value="21" <?php if($fn_time_val == "21") echo "selected"; ?> >9 pm</option>
        <option value="22" <?php if($fn_time_val == "22") echo "selected"; ?> >10 pm</option>
        <option value="23" <?php if($fn_time_val == "23") echo "selected"; ?> >11 pm</option>
    </select>
</p>

<?php
   

        for($i=0; $i < $num_schools; $i++){
            ?>
            <div style="float:left; margin-right:10px;"><h3><?php _e($schools[$i]); ?></h3>
            <p style="float:left">Usernames<br><textarea name='<?php echo $sanitized_schools[$i]."_users"; ?>'><?php echo $schools_user_val[$i];?></textarea></p>
            <p style="float:left">Passwords<br><textarea name='<?php echo $sanitized_schools[$i]."_pass"; ?>'><?php echo $schools_pass_val[$i];?></textarea></p></div>
            <?php
        }

?>

<div style="clear:both"></div>
<div style="float:left; margin-right:10px;">
<h3><?php _e("Append to Student Email"); ?></h3>
<p><textarea class="largeText" name="<?php echo $email_append; ?>"><?php echo $email_append_val;?></textarea></p>
</div>
<div style="float:left; margin-right:10px;">
<h3><?php _e("Tutor Reminder Email Subject"); ?></h3>
<p><input type="text" name="<?php echo $reminder_subject; ?>" value="<?php echo $reminder_subject_val;?>"></p>
<h3><?php _e("Tutor Reminder Email Body"); ?></h3>
<p><textarea class="largeText" name="<?php echo $reminder_body; ?>"><?php echo $reminder_body_val;?></textarea></p>
</div>
<div style="float:left; margin-right:10px;">
<h3><?php _e("Student Reminder Email Subject"); ?></h3>
<p><input type="text" name="<?php echo $student_reminder_subject; ?>" value="<?php echo $student_reminder_subject_val;?>"></p>
<h3><?php _e("Student Reminder Email Body"); ?></h3>
<p><textarea class="largeText" name="<?php echo $student_reminder_body; ?>"><?php echo $student_reminder_body_val;?></textarea></p>
</div>
<div style="clear:both"></div>
<hr />
</div>
<p class="submit">
<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
</p>
</form>
<?php do_shortcode("[admin-appointments]"); do_shortcode("[admin-appointments next_week='true']"); ?>
<h1>Your Filled Appointments</h1>
<?php
do_shortcode("[admin-registered-appointments]");
?>
</div>
<?php
 
}


function appt_install () {
   global $wpdb;
   $table_name = $wpdb->prefix . "online_appt"; 

   $sql = "CREATE TABLE $table_name (
	  id mediumint(9) NOT NULL AUTO_INCREMENT,
	  owner tinytext NOT NULL,
      owner_picked tinytext NOT NULL,
	  time_stamp datetime DEFAULT '00-00-0000 00:00:00' NOT NULL,
	  appt_length time DEFAULT '00:00:00' NOT NULL,
	  appt_time time DEFAULT '00:00:00' NOT NULL,
	  appt_date date DEFAULT '00-00-0000' NOT NULL,
	  attendee tinytext NOT NULL,
	  subject tinytext NOT NULL,
	  comment text NOT NULL,
      username text NOT NULL,
      password text NOT NULL,
      owner_cron text NOT NULL,
      attendee_cron text NOT NULL,
	  UNIQUE KEY id (id)
	);";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);
}

function appt_install_data () {

    add_option('days_to_show');
    add_option('appt_start_time');
    add_option('appt_finish_time');


    //default to show only weekdays, 4pm start and 10pm end
    update_option('days_to_show', 'no');
    update_option('appt_start_time', '16');
    update_option('appt_finish_time', '22');

}

function days_ahead($days) {
    $d = explode('-', date('H-i-s-n-j'));
    return date('Y-m-d H:i:s', mktime($d[0], $d[1], $d[2], $d[3], $d[4]+$days));
}


register_activation_hook(__FILE__,'appt_install');
register_activation_hook(__FILE__,'appt_install_data');


//[appointments]
function cal_display( $atts ){

ob_start();
    global $wpdb;
    global $current_user;
    extract( shortcode_atts( array('next_week' => 'false'), $atts));
if(is_user_logged_in()){


    $user_name = $current_user->user_login;

    $val = get_option('days_to_show');
    $st_time = get_option('appt_start_time');
    $fn_time = get_option('appt_finish_time');
    $table_name = $wpdb->prefix . "online_appt";
    

    if($val == "yes"){$val = 7;} else{$val=5;}
    $this_week = week_from_monday($val, $next_week );

    $table_class = "appt-cal";

    if($next_week == "false"){
	
?>
<div class='registration'>
    <form id='registration-form' class='reg-form' method='post' action='<?php echo plugins_url('register.php', __FILE__); ?>'>
        <p><label for=owners>Select your tutor:</label>
            <select name='owners' id='owners'>
            </select>
        </p>
        <p><label for=subject>In what subject would you like help?<span class="required"> (required)</span></label>
            <input type='text' name='subject' id='subject' value=''>
        </p>
        <p><label for=comment>Please leave a comment about what you need help with<span class="required"> (required)</span></label>
            <br><textarea name='comment' id='comment'></textarea>
            <input type=hidden name='appt_time' id='appt_time' value=''>
            <input type=hidden name='appt_date' id='appt_date' value=''>
        </p>

        <p><input type=submit value="Register"></p>
    </form>
</div>
<?php
    if(get_query_var('appt_error') == 'duplicate'){ ?>

    <p class="error">ERROR: You may only sign up for one appointment each day</p>

<?php    }
?>
<?php
    if(get_query_var('appt_error') == 'logins'){ ?>

    <p class="error">ERROR: We have run out of logins for the day you have chosen. Please choose a time slot on another day.</p>

<?php    }
?>
<?php
    if(get_query_var('appt_error') == 'school'){ ?>

    <p class="error">ERROR: There has been an error when looking up your school. Please contact wolverine.tutors@umich.edu.</p>

<?php    }
?>
<div><a id="cal_shift" href=#  data-words="Last Week" >Next Week</a></div>
<div id="cal_container">
<?php
}
else{
    $table_class = "appt-cal-next";
}
?>

<table class="<?php echo $table_class; ?> appt-table">
    <thead>
        <tr>
        <th></th>
<?php

    foreach($this_week as $date){
        list($year, $month, $day) = explode("-", $date);
        echo "<th>".date("D dS", mktime('0','0','0',$month,$day,$year))."</th>";
    }
?>
        </tr>
    </thead>
    <tbody>
    <?php
        //build the appointment option table
        for($i = $st_time; $i<=$fn_time; $i++){
            if($i-12 <= 0){$des = "am"; $time = $i;}else{$des = "pm"; $time = $i-12;}
            echo "<tr><td>".$time." ".$des."</td>";
            for($j = 1; $j<=$val; $j++){
                $cur_date = $this_week[$j-1];
                $cur_time = date('H:i:s', strtotime($time." ".$des));
                $owners = $wpdb->get_results($wpdb->prepare("SELECT owner FROM $table_name WHERE appt_time = %s AND appt_date = %s", $cur_time, $cur_date));
                $picked = $wpdb->get_results($wpdb->prepare("SELECT owner_picked FROM $table_name WHERE appt_time = %s AND appt_date = %s", $cur_time, $cur_date));
                $attendees = $wpdb->get_results($wpdb->prepare("SELECT attendee FROM $table_name WHERE appt_time = %s AND appt_date = %s", $cur_time, $cur_date));
                //need to explode owners check against exploded picked and delete anyone that is in both.
                if($picked[0]->owner_picked != NULL){
                    $owners_arr = explode(',', $owners[0]->owner);
                    $picked_arr = explode(',', $picked[0]->owner_picked);
                    foreach ($owners_arr as $key => $own) {
                        foreach ($picked_arr as $pick_key => $pick) {
                            if ($own == $pick){
                                unset($owners_arr[$key]);
                            }
                        }
                    }

                    $owners[0]->owner = implode(',', $owners_arr);
                }




                echo "<td><a href=# data-time='".$cur_time."' data-date='".$cur_date."' ";
                if($owners == NULL || $owners[0]->owner == ""){echo "class=not-avail"; $owner_count = 0;}
                //execute sql query to determine if this day, time and designation are matched to any signed up tutors
                
                else{
                    
                    $owner_array = explode(',', $owners[0]->owner);


                    if($attendees[0]->attendee != NULL){
                        $attendee_array = explode(',', $attendees[0]->attendee);
                        $attendee_count = count($attendee_array);
                    }
                    else{
                        $attendee_count = 0;
                    }

                    $owner_count = count($owner_array);

                    if($owner_count < $attendee_count) echo "class=not-avail ";
                    else{
                        $owners_displays = array();
                        for($k=0; $k<count($owner_array); $k++){
                            $user_info = get_user_by('login', $owner_array[$k]);
                            array_push($owners_displays, $user_info->display_name);
                        }
                        $owners_displays = implode(',', $owners_displays);
                        echo "data-owners='".$owners[0]->owner."' data-displays='".$owners_displays."'";
                    }
                }
                 echo "></a></td>";
            }
            echo "</tr>";

        }
    ?>
    </tbody>
</table>

<?php
    if($next_week == "true"){
        echo "</div>";
    }
	 
}
elseif($next_week == "false"){
    display_appt_login();
}
return ob_get_clean();
}


//[admin-appointments]
function admin_cal_display( $atts ){
    global $wpdb;
    global $current_user;
    $user_name = $current_user->user_login;

    $val = get_option('days_to_show');
    $st_time = get_option('appt_start_time');
    $fn_time = get_option('appt_finish_time');
    $table_name = $wpdb->prefix . "online_appt";
    extract( shortcode_atts( array('next_week' => 'false'), $atts));

    if($val == "yes"){$val = 7;} else{$val=5;}
    $this_week = week_from_monday($val, $next_week );

    $table_class = "appt-cal";

    if($next_week == "true") $table_class = "appt-cal-next-admin";
?>
<table class="<?php echo $table_class; ?> appt-table">
    <thead>
        <tr>
        <th></th>
<?php

    foreach($this_week as $date){
        list($year, $month, $day) = explode("-", $date);
        echo "<th>".date("D dS", mktime('0','0','0',$month,$day,$year))."</th>";
    }
?>
        </tr>
    </thead>
    <tbody>
    <?php
        //build the appointment option table
        for($i = $st_time; $i<=$fn_time; $i++){
            if($i-12 <= 0){$des = "am"; $time = $i;}else{$des = "pm"; $time = $i-12;}
            echo "<tr><td>".$time." ".$des."</td>";
            for($j = 1; $j<=$val; $j++){
                $cur_date = $this_week[$j-1];
                $cur_time = date('H:i:s', strtotime($time." ".$des));
                $result = $wpdb->get_results($wpdb->prepare("SELECT owner FROM $table_name WHERE appt_time = %s AND appt_date = %s", $cur_time, $cur_date));
                echo "<td><a ";
                if($result != NULL) echo "class=not-avail";
                //execute sql query to determine if this day, time and designation are matched to any signed up tutors
                 echo " href=# data-time='".$cur_time."' data-date='".$cur_date."' >";
                if($result != NULL){
                    $owners = explode(',', $result[0]->owner);
                    $count = count($owners);
                    foreach($owners as $owner){
                        if($user_name == $owner) {
                            if($count > 1) $count = "You + ".($count-1);
                            else $count = "You";
                        }
                    }
                    echo $count;
                }
                 echo "</a></td>";
            }
            echo "</tr>";

        }
    ?>
    </tbody>
</table>
<?php

}




function week_from_monday($days, $next, $weekend) {
      $gofs = get_option( 'gmt_offset' ); // get WordPress offset in hours
  $tz = date_default_timezone_get(); // get current PHP timezone
  date_default_timezone_set('Etc/GMT'.(($gofs < 0)?'+':'').-$gofs); // set the PHP timezone to match WordPress
    $date = date("Y-m-d");

    if($next == "true"){$date = date("Y-m-d", strtotime("+1 week"));}

    list($year, $month, $day) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0',$month,$day,$year));
//echo $wkday;
    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }

    if($numDaysToMon > 4 && $days < 6){
        if($next == "true"){ $date = date("Y-m-d", strtotime("+2 weeks")); }
        else{$date = date("Y-m-d", strtotime("+1 week")); }
        list($year, $month, $day) = explode("-", $date);
    }

    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for opt days from Monday (inclusive)
    for($i=0; $i<$days; $i++)
    {
        $dates[$i] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
    }
      date_default_timezone_set($tz); // set the PHP timezone back the way it was
    return $dates;
}

function appointments_display(){
if(is_user_logged_in()){
ob_start();
    global $wpdb;
    global $current_user;
    $user_name = $current_user->user_login;

    $table_name = $wpdb->prefix . "online_appt";
    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE DATE(appt_date) >= DATE(NOW()) "));
?>
<table class='registered'>
    <thead>
        <tr><th>Tutor</th><th>Time</th><th>Date</th><th>Username</th><th>Password</th><th></th></tr>
    </thead>
    <tbody>
<?php

    $len = count($result);

    for($i = 0; $i<$len; $i++){
        $owners = $result[$i]->owner_picked;
        $attendees = $result[$i]->attendee;
        $passwords = $result[$i]->password;
        $usernames = $result[$i]->username;
        $owners = explode(',', $owners);
        $attendees = explode(',', $attendees);
        $passwords = explode(',', $passwords);
        $usernames = explode(',', $usernames);
        $at_len = count($attendees);
        for($j=0; $j<$at_len; $j++){
            if($attendees[$j] == $user_name){
                $user_info = get_user_by('login', $owners[$j]);
                $display_name = $user_info->display_name;
                ?>
                <tr>
                    <form action="<?php echo plugins_url('de-register.php', __FILE__); ?>" method="post">
                        <td><?php echo $display_name; ?><input type=hidden name=tutor value="<?php echo $owners[$j]; ?>"></td>
                        <td><?php echo $result[$i]->appt_time; ?><input type=hidden name=time value=<?php echo $result[$i]->appt_time; ?>></td>
                        <td><?php echo $result[$i]->appt_date; ?><input type=hidden name=date value=<?php echo $result[$i]->appt_date; ?>></td>
                        <td><?php echo trim($usernames[$j]); ?></td>
                        <td><?php echo trim($passwords[$j]); ?></td>
                        <td><input type=submit value=Remove></td>
                    </form>
                </tr>
                <?php
            }
        }

    }
?>
    </tbody>
</table>
<?php
}
else{
    display_appt_login();
}
return ob_get_clean();
}

function admin_appointments_display(){
    global $wpdb;
    global $current_user;
    $user_name = $current_user->user_login;
    $display_name = $current_user->display_name;

    $table_name = $wpdb->prefix . "online_appt";
    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name "));
?>
<table class='registered '>
    <thead>
        <tr><th>Student</th><th>Time</th><th>Date</th></tr>
    </thead>
    <tbody>
<?php

    $len = count($result);

    for($i = 0; $i<$len; $i++){
        $owners = $result[$i]->owner_picked;
        $attendees = $result[$i]->attendee;
        $owners = explode(',', $owners);
        $attendees = explode(',', $attendees);
        $at_len = count($attendees);
        for($j=0; $j<$at_len; $j++){
            if($owners[$j] == $user_name){
                echo "<tr><td>".$attendees[$j]."</td>";
                echo "<td>".$result[$i]->appt_time."</td>";
                echo "<td>".$result[$i]->appt_date."</td></tr>";
            }
        }

    }
?>
    </tbody>
</table>
<?php
}

function display_appt_login(){
    ?>
        <h2>It doesn't look like you are logged in :(</h2>
        <p>You must be logged in to use this functionality.</p>
        <div class='login-block'>
            <p><a href='<?php echo home_url()."/wp-login.php"; ?>'>Login or Register Here</a></p>
        </div>
    <?php
}

add_filter('query_vars', 'add_my_var');

function add_my_var($public_query_vars) {
    $public_query_vars[] = 'appt_error';
    return $public_query_vars;
}

function online_appt_cron_emails($today){
    global $wpdb;
    $table_name = $wpdb->prefix . "online_appt";
    $gofs = get_option( 'gmt_offset' ); // get WordPress offset in hours
    $tz = date_default_timezone_get(); // get current PHP timezone
    date_default_timezone_set('Etc/GMT'.(($gofs < 0)?'+':'').-$gofs); // set the PHP timezone to match WordPress
    $tomorrow = date("Y-m-d",strtotime("+1 day"));
    if($today){
        $tomorrow = date("Y-m-d", time());
    }

	date_default_timezone_set($tz); // set the PHP timezone back the way it was
    $empty = "";
    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE appt_date = %s AND owner_picked <> %s", $tomorrow, $empty));

    $email_data = array();
    for($i=0; $i<count($result); $i++){
        get_email_data($email_data, $result[$i], $today);
    }

    $tutor_email_subject = get_option('online_appt_cron_email_subject');
    $tutor_email_body = get_option('online_appt_cron_email_body');
    $student_email_subject = get_option('online_appt_cron_email_subject_student');
    $student_email_body = get_option('online_appt_cron_email_body_student');


    add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
    for($i=0; $i<count($email_data); $i++){
        

        $already_sent = false;
        $tutor_login = $email_data[$i]['tutor_login'];
        for($j=0; $j<count($email_data[$i]['student_emails']); $j++){
            $appt_date = $email_data[$i]['date'];
            $appt_time = $email_data[$i]['times'][$j];

            $current_row = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE appt_date = %s AND appt_time = %s", $appt_date, $appt_time));

            $attend_cron = explode(',',$current_row[0]->attendee_cron);
            $owner_cron = explode(',',$current_row[0]->owner_cron);

            $student_login = $email_data[$i]['student_login'][$j];

            if(!in_array($student_login,$attend_cron)){ //if we have not sent an email to this person
                $student_body = parse_email_body($student_email_body, $email_data[$i], true, $j);
				//echo $email_data[$i]['student_emails'][$j];
                if(wp_mail( $email_data[$i]['student_emails'][$j], "[Wolverine Tutors] ".$student_email_subject, $student_body)){

                    array_push($attend_cron, $student_login);

                    $update = array('attendee_cron' => implode(',',$attend_cron));

                    $where = array('appt_time' => $appt_time ,'appt_date' => $appt_date );
                    $wpdb->update($table_name, $update, $where);
                }

            }

            if(!in_array($tutor_login, $owner_cron) && !$already_sent){
                $tutor_body = parse_email_body($tutor_email_body, $email_data[$i]);
                //echo $email_data[$i]['email'];
                if(wp_mail( $email_data[$i]['email'], "[Wolverine Tutors] ".$tutor_email_subject, $tutor_body)){
                    //set tutor_sent
                    array_push($owner_cron, $tutor_login);
                    $update = array('owner_cron' => implode(',',$owner_cron));

                    $where = array('appt_time' => $appt_time,'appt_date' => $appt_date );
                    $wpdb->update($table_name, $update, $where);
                    $already_sent = true;
                }          
            }
            else if($already_sent){
                array_push($owner_cron, $tutor_login);
                $update = array('owner_cron' => implode(',',$owner_cron));

                $where = array('appt_time' => $appt_time,'appt_date' => $appt_date );
                $wpdb->update($table_name, $update, $where);
            }
        }
    }


    return true;
}

function get_email_data(&$emails, $result, $today){
    global $wpdb;
    $picked = explode(',', $result->owner_picked);
    $attendees = explode(',',$result->attendee);
    $usernames = explode(',',$result->username);
    $passwords = explode(',',$result->password);



    for($j=0; $j<count($picked); $j++){
        $user_info = get_user_by('login', $picked[$j]);
        $attend_info = get_user_by('login', $attendees[$j]);
        $attend_meta = get_user_meta($attend_info->ID);
        if(trim($attend_meta['first_name'][0]) == ""){
            $attend_name = $attend_info->display_name;
        }
        else{
            $attend_name = $attend_meta['first_name'][0]." ".$attend_meta['last_name'][0];
        } 
        if($today){
            $day_text = "today";
        }
        else{
            $day_text = "tomorrow";
        }

        $email = array(
            "email" => $user_info->user_email,
            "name" => $user_info->display_name,
            "date" => $result->appt_date,
            "day_text" => $day_text,
            "times" => array($result->appt_time),
            "tutor_login" => $picked[$j],
            "student_emails" => array($attend_info->user_email),
            "student_login" => array($attendees[$j]),
            "student_names" => array($attend_name),
            "passwords" => array($passwords[$j]),
            "usernames" => array($usernames[$j])
            );




        $duplicate = false;
        for($k=0; $k<count($emails); $k++){
            if($emails[$k]['email'] == $email['email']){
                $duplicate = true;
                array_push($emails[$k]['times'], $email['times'][0]);
                array_push($emails[$k]['student_login'], $email['student_login'][0]);
                array_push($emails[$k]['passwords'], $email['passwords'][0]);
                array_push($emails[$k]['usernames'], $email['usernames'][0]);
                array_push($emails[$k]['student_emails'], $email['student_emails'][0]);
                array_push($emails[$k]['student_names'], $email['student_names'][0]);

                break;
            }
        }
        if(!$duplicate){
            array_push($emails,$email);
        }


    }
}

function parse_email_body($body, $data, $student = false, $index){
    $new_body = nl2br($body);

    if($student){
        $new_body = str_replace("[time]", $data['times'][$index], $new_body);
        $new_body = str_replace("[recipient]", $data['student_names'][$index], $new_body);
        $new_body = str_replace("[username]", $data['usernames'][$index], $new_body);
        $new_body = str_replace("[password]", $data['passwords'][$index], $new_body);
    }
    else{
        $new_body = str_replace("[time]", implode(' & ', $data['times']), $new_body);
        $new_body = str_replace("[recipient]", $data['name'], $new_body);
        $new_body = str_replace("[info_table]", info_table($data['student_names'], $data['usernames'], $data['times']), $new_body);
    }
    
    $new_body = str_replace("[date]", $data['date'], $new_body);
    $new_body = str_replace("[tutor_name]", $data['name'], $new_body);
    $new_body = str_replace("[tutor_email]", $data['email'], $new_body);
    $new_body = str_replace("[today_tomorrow]", $data['day_text'], $new_body);


    return $new_body;
}

function info_table($student_names, $usernames, $times){
    $html = "<table style='width:500px'>";
    $html .= "<thead><tr><th>Time</th><th>Student</th><th>Username</th></tr></thead>";
    $html .= "<tbody style='text-align:center'>";
    for($i=0; $i<count($student_names); $i++){
        $html .= "<tr>";
        $html .= "<td>".$times[$i]."</td>";
        $html .= "<td>".$student_names[$i]."</td>";
        $html .= "<td>".$usernames[$i]."</td>";
        $html .= "</tr>";
    }
    $html .= "</tbody></table>";
    return $html;
}

add_shortcode( 'appointments', 'cal_display' );
add_shortcode( 'admin-appointments', 'admin_cal_display' );
add_shortcode( 'registered-appointments', 'appointments_display' );
add_shortcode( 'admin-registered-appointments', 'admin_appointments_display' );


?>
